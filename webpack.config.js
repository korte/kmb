const path = require('path');

module.exports = {
	context: path.join(__dirname, 'src'),
	entry: './app.js',
	output: {
		path: __dirname,
		filename: 'bundle.js'
	},
	devServer: {
		contentBase: __dirname
	},
	module: {
		loaders: [
			{
				test: /src.*\.js$/,
				use: [{ loader: 'ng-annotate-loader', options: { ngAnnotate: "ng-annotate-patched", explicitOnly: false } }, 'babel-loader']
            },
			{
				test: /\.(css|scss)$/,
				loader: 'style-loader!css-loader!sass-loader'
            },
			{
				test: /\.(eot|svg|ttf|woff|woff2)$/,
				loader: 'file-loader?name=public/fonts/[name].[ext]'
            }
        ]
	}
}
