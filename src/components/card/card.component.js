import "./card.scss";

class CardController {
	constructor() {
		this.defaultImgUrl = "./src/assets/img/news-default.png";
	}

	$onInit() {}

	$onChanges(changesObj) {}

	$onDestroy() {}
}
export default {
	templateUrl: "./src/components/card/card.html",
	controller: CardController,
	bindings: {
		title: '@',
		description: '@',
		imageUrl: '@',
		tag: "@"
	}
};
