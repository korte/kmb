import "./app-footer.styles.scss";

class AppFooterController {
    constructor() {}
}
export default {
    templateUrl: "./src/components/app-footer/app-footer.html",
    controller: AppFooterController,
    bindings: {}
};