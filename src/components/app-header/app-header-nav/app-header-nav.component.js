import styles from "./app-header-nav.scss";

class AppHeaderNavController {
	constructor() {}
}

export default {
	templateUrl: "./src/components/app-header/app-header-nav/app-header-nav.html",
	controller: AppHeaderNavController,
	bindings: {
		avatarImgUrl: '@'
	}
};
