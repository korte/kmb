import styles from "./app-header.scss";

class AppHeaderController {
	constructor() {
		this.avatarImgUrl = "./src/assets/img/avatar.png";
		this.coverImgUrl = "./src/assets/img/cover.jpg";
	}
}

export default {
	templateUrl: "./src/components/app-header/app-header.html",
	controller: AppHeaderController,
	bindings: {}
};
