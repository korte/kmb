import "./searchbox.scss";

class SearchboxController {
    constructor() {}

    $onInit() {}

    $onChanges(changesObj) {}

    $onDestroy() {}
}
export default {
    templateUrl: "./src/components/searchbox/searchbox.html",
    controller: SearchboxController,
    bindings: {
        onSearch:'&',
        placeholder: '@'
    }
};