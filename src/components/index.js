import angular from "angular";

import login from "./login/login.component";
import app from "./app/app.component";

import news from "./news/news.component";

import appHeader from "./app-header/app-header.component";
import appHeaderNav from "./app-header/app-header-nav/app-header-nav.component";
import appFooter from "./app-footer/app-footer.component";

import searchbox from "./searchbox/searchbox.component";
import selectbox from "./selectbox/selectbox.component";
import card from "./card/card.component";
import pagination from "./pagination/pagination.component";

const components = angular.module("components", [])

components
    .component('login', login)
    .component('app', app)
    .component('news', news)
    .component('appHeader', appHeader)
    .component('appHeaderNav', appHeaderNav)
    .component('appFooter', appFooter)
    .component('card', card)
    .component('pagination', pagination)
    .component('selectbox', selectbox)
    .component('searchbox', searchbox);

export default components;