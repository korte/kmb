import "./app.scss";

class AppController { 
    constructor() {} 


     $onInit() {}

     $onChanges(changesObj) {}

     $onDestroy() {}
}
export default {
    templateUrl: "./src/components/app/app.html" , 
    controller: AppController,
    bindings: {}
};