import "./selectbox.scss";

class SelecboxController {
	constructor() {}

	$onInit() {}

	$onChanges(changesObj) {}

	$onDestroy() {}
}
export default {
	templateUrl: "./src/components/selectbox/selectbox.html",
	controller: SelecboxController,
	bindings: {
		placeholder: "@",
		options: "<",
		onChange: "&"
	}
};
