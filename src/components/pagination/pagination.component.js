import "./pagination.scss";
import * as _ from "lodash";

class PaginationController {
	constructor() {}

	$onChanges(changesObj) {
		const totalRecordsChanged = changesObj.totalRecords && !_.isNil(changesObj.totalRecords.currentValue);
		if (totalRecordsChanged) {
			this.initTotalPages();
		}
	}

	onManualPageChange(previousPage) {
		const pageValid = this.currentPage > 0 && this.currentPage <= this.totalPages;
		console.log(pageValid);
		if (!pageValid) {
			this.currentPage = previousPage;
			return;
		};
		this.onPageChange({ page: parseInt(this.currentPage) });
	}

	initTotalPages() {
		this.totalPages = Math.ceil(this.totalRecords / this.pageSize);
	}
}
export default {
	templateUrl: "./src/components/pagination/pagination.html",
	controller: PaginationController,
	bindings: {
		currentPage: '<',
		totalRecords: '<',
		pageSize: '<',
		onPageChange: '&'
	}
};
