import "./news.scss";
import * as _ from "lodash";

class NewsController {
	constructor(api) {
		this.api = api;
		this.pageSize = 6;
		this.currentPage = 1;
		this.noResultsImg = "./src/assets/img/no-results.png"
	}

	$onInit() {
		this.getSources();
	}

	getSources() {
		this.api.getSources()
			.then(resp => {
				this.sources = resp.data.sources;
				this.categories = _.uniqBy(resp.data.sources, 'category').map(source => source.category);
			})
			.catch(e => console.log(e));

	}

	changePage(page) {
		this.currentPage = page;
	}

	updateSearchModel(query) {
		this.searchQuery = query;
		this.changePage(1);
	}

	changeCategory(category) {
		this.selectedCategory = category;
		this.changePage(1);
	}

}
export default {
	templateUrl: "/src/components/news/news.html",
	controller: NewsController,
	bindings: {}
};
