class StartFrom {
	constructor() {
		return function(data, start) {
			if (start === 0) return data;
			return data.slice(start);
		}
	};
};

export default StartFrom;


