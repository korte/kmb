import angular from "angular";

import startFrom from "./startFrom.filter";
import capitalize from "./capitalize.filter";

const filters = angular.module('filters', []);

filters
	.filter('capitalize', capitalize)
	.filter('startFrom', startFrom);

export default filters;
