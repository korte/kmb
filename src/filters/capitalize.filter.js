class Capitalize {
	constructor() {
		return function(data) {
			const isArray = Array.isArray(data);
			if (!isArray) data = [data];
			data = data.map(str => {
				if (!str) return;
				str = str.toLowerCase();
				str = str.charAt(0).toUpperCase() + str.slice(1);
				return str;
			});
			return isArray ? data : data[0];
		}
	};
};

export default Capitalize;
