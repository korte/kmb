class Api {
	constructor($http) {
		this.$http = $http;
		this.apiKey = "78c6f94715e249e18f02c4b93e8f963e";
		this.baseUrl = "https://newsapi.org/v2";
		this.$http = $http;
	};

	getSources() {
		return this.$http.get(this.baseUrl + '/sources', {
			params: {
				apiKey: this.apiKey
			}
		})
	};

};

export default Api;
