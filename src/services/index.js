import angular from "angular";
import api from "./api.service";

const services = angular.module('services',[]);

services.service("api",api);

export default services;