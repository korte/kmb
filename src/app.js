'use strict';

import styles from "./styles/index.scss"


import angular from "angular";
import ngTouch from "angular-touch"
import ngAnimate from "angular-animate"
import uiBootstrap from "angular-ui-bootstrap"


import core from "./core";
import components from "./components";
import services from "./services";
import filters from "./filters";

const ngModule = angular.module('newsApp', [ngTouch, ngAnimate, uiBootstrap, core.name, components.name, services.name, filters.name]);

ngModule.component('newsApp', {
  template: `
              <ui-view></ui-view>
            `
});

document.addEventListener('DOMContentLoaded', () => angular.bootstrap(document, ['newsApp']));