import angular from "angular";
import Config from "./app.config";
import uiRouter from "angular-ui-router";


const core = angular.module('core',[uiRouter]);

core.config(Config);

export default core;