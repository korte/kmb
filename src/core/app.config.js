class Config {
	  /*@ngInject*/
	constructor($stateProvider, $urlRouterProvider) {
		this.$stateProvider = $stateProvider;
		this.$urlRouterProvider = $urlRouterProvider;
		this.routes = [{
			name: "login",
			config: {
				url: '/login',
				template: '<login></login>'
			}
        }, {
			name: "app",
			config: {
				url: '/app',
				abstract: true,
				template: '<app></app>'
			}
        }, {
			name: "news",
			config: {
				parent: 'app',
				url: '/news',
				template: '<news></news>'
			}
        }];
		this.initializeRoutes();
	};

	initializeRoutes() {
		this.routes.forEach(route => {
			this.$stateProvider.state(route.name, route.config);
		});
		this.$urlRouterProvider.otherwise('/login');
	};

}

export default Config;
